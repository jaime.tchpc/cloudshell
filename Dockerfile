FROM ubuntu:jammy
LABEL maintainer="jim2k7@gmail.com"

RUN set -eux; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    curl \
    ubuntu-cloud-keyring;

COPY <<EOF /etc/apt/sources.list.d/cloudarchive-bobcat.list
deb [arch=amd64] http://ubuntu-cloud.archive.canonical.com/ubuntu jammy-updates/bobcat main
# deb-src http://ubuntu-cloud.archive.canonical.com/ubuntu jammy-updates/bobcat main
EOF

RUN set -eux; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    python3-openstackclient; \
    apt-get clean && rm -rf /var/lib/apt/lists/* ;

RUN set -eux; \
    curl -sSL https://raw.githubusercontent.com/gbraad/openstack-tools/master/stack -o /usr/local/bin/stack; \
    chmod +x /usr/local/bin/stack;

RUN mkdir -p /root/.stack /root/.config/openstack /workspace
VOLUME ["/root/.stack", "/root/.config/openstack", "/workspace"]
WORKDIR /workspace

CMD ["/usr/bin/bash"]